# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][keep-a-changelog] and this project adheres to [Semantic
Versioning][semantic-versioning].


## [Unreleased]
## [0.3.0] - 2019-12-05
### Changed

* Pinning exact versions in [`requirements.txt`](requirements.txt).
    * Upgraded Poetry to version 1.0.0b8.
    * Upgraded md-toc to version 6.0.0.

## [0.2.0] - 2019-06-26
### Added

* The [`poetry-export`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.2.0/README.md#poetry-export) hook.
* Using the `md-toc` hook in this repository itself.

## [0.1.1] - 2019-05-14
### Changed

* The [`md-toc`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.1/README.md#md-toc) hook no longer fails if no files are provided.

## [0.1.0] - 2019-05-14
### Added

* Features
    * The [`md-toc`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/README.md#md-toc) hook.
* Files
    * [`.bumpversion.cfg`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/.bumpversion.cfg) &mdash; Configuration file for the [bumpversion][bumpversion] version-tagging package.
    * [`.gitignore`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/.gitignore) &mdash; List of files and directories paths/patterns [ignored by Git][gitignore].
    * [`.gitlab-ci.yml`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/.gitlab-ci.yml) &mdash; Continuous integration/deploy configuration ([GitLab CI][gitlab-ci]), featuring:
        * Source-code linting provided by [Black][pip-black].
        * [Security verification][repository-security] of Python dependencies (via [Gitlab Dependency Scanning][gitlab-dependency-scanning])
    * [`.pre-commit-hooks.yaml`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/.pre-commit-hooks.yaml) &mdash; Configuration file for the [pre-commit][pre-commit] hooks published by this package.
    * [`.pre-commit-config.yaml`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/.pre-commit-config.yaml) &mdash; Configuration file for [pre-commit][pre-commit] hooks used. Includes:
        * Automatic code styling with [Black][pip-black].
    * [`CHANGELOG.md`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/CHANGELOG.md) &mdash; this very history file, which follows the [Keep a Changelog][keep-a-changelog] standard.
    * [`LICENSE.txt`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/LICENSE.txt) &mdash; Copy of the [MIT license][license-mit] (a permissive [open source license][open-source]).
    * [`README.md`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/README.md) &mdash; repository front-page.
    * [`hooks`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/hooks) &mdash; [Python 3.6][python-docs] package, where each module implements a [pre-commit][pre-commit] hook.
    * [`requirements.txt`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/requirements.txt) &mdash; [Python][python-docs] requirements file.
    * [`setup.py`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/setup.py) &mdash; package [setup script][setup.py].
    * [`setup.cfg`](https://gitlab.com/psa-exe/python-pre-commit/blob/0.1.0/setup.cfg) &mdash; package [setup options][setup.cfg].


[Unreleased]: https://gitlab.com/psa-exe/python-pre-commit/compare?from=release&to=master
[0.3.0]: https://gitlab.com/psa-exe/python-pre-commit/compare?from=0.2.01&to=0.3.0
[0.2.0]: https://gitlab.com/psa-exe/python-pre-commit/compare?from=0.1.1&to=0.2.0
[0.1.1]: https://gitlab.com/psa-exe/python-pre-commit/compare?from=0.1.0&to=0.1.1
[0.1.0]: https://gitlab.com/psa-exe/python-pre-commit/tags/0.1.0

[bumpversion]: https://github.com/peritus/bumpversion
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[gitignore]: https://git-scm.com/docs/gitignore 
[gitlab-ci]: https://docs.gitlab.com/ee/ci
[gitlab-dependency-scanning]: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html
[gitlab-license-management]: https://docs.gitlab.com/ee/user/application_security/license_management/index.html
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/
[license-mit]: https://opensource.org/licenses/MIT
[pip-black]: https://pypi.org/project/black/
[pre-commit]: https://pre-commit.com/
[python-docs]: https://docs.python.org/3.6/
[python-pytest]: https://pytest.org/
[repository-licenses]: https://gitlab.com/psa.exe/python-pre-commit/licenses/dashboard
[repository-security]: https://gitlab.com/psa.exe/python-pre-commit/security/dashboard
[semantic-versioning]: https://semver.org/spec/v2.0.0.html
[setup.cfg]: https://setuptools.readthedocs.io/en/latest/setuptools.html#configuring-setup-using-setup-cfg-files
[setup.py]: https://docs.python.org/3.7/distutils/setupscript.html
