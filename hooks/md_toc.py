import argparse
import md_toc
import sys


def write_tocs(filenames):
    if filenames:
        for filename in filenames:
            print(f'Generating TOC for file "{filename}"')
            toc_string = md_toc.build_toc(filename=filename, parser="gitlab")
            print(f'Writing down TOC for file "{filename}"')
            md_toc.write_string_on_file_between_markers(
                filename=filename, string=toc_string, marker="[](TOC)"
            )
    else:
        print("No files, nothing to do")
    return 0


def main(argv=None):
    print("Running the md-TOC pre-commit hook")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "filenames", nargs="*", help="Names of markdown (.md) files to be modified"
    )
    args = parser.parse_args(argv)
    return write_tocs(filenames=args.filenames)


if __name__ == "__main__":
    sys.exit(main(argv=None))
