import argparse
from subprocess import PIPE, Popen, TimeoutExpired


def export_requirements(include_dev):
    command = f"poetry export -f requirements.txt"
    if include_dev:
        command += " --dev"
    com_ret = run_process(command, stdout=PIPE)
    git_ret = run_process("git diff --exit-code requirements.txt")
    return com_ret or git_ret


def main(argv=None):
    print("Running the poetry-export pre-commit hook")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dev",
        "-d",
        action="store_const",
        const=True,
        default=False,
        help="Wether to export dev-deps.",
    )
    args = parser.parse_args(argv)
    return export_requirements(include_dev=args.dev)


def run_process(command, **options):
    proc = Popen(command.split(), **options)
    try:
        outs, errs = proc.communicate(timeout=30)
    except TimeoutExpired:
        proc.kill()
        outs, errs = proc.communicate()
    if outs:
        print(outs)
    if errs:
        print(f"[ERROR]: {errs}")
    return proc.returncode


if __name__ == "__main__":
    sys.exit(main(argv=None))
