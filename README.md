Useful Python-based pre-commit hooks for Git
============================================

[![][badge-python]][python-docs]
[![][badge-version]][repository-latest-release]

[![][badge-mit]][license-mit]
[![][badge-black]][pip-black]

[![-security][badge-ci-security]][repository-security]

This repository contains a [Python 3.6][python-docs] package that implements a series of [Git hooks][git-hooks]
compatible with the [pre-commit][pre-commit] system. 

**Table of contents:**

[](TOC)

- [Usage](#usage)
- [Hooks list](#hooks-list)
  - [md-toc](#md-toc)
  - [poetry-export](#poetry-export)
- [Future directions](#future-directions)

[](TOC)

---

# Usage

Install [pre-commit][pre-commit] on your repository's [virtualenv][virtualenv] and set it up with:

```bash
pip install pre-commit
pre-commit install
```

Add the following to your `.pre-commit-config.yaml`:

```yaml
repos:
  - repo: https://gitlab.com/psa-exe/python-pre-commit
    rev: 0.2.0
```

See the [Hooks list](#hooks-list) section for available hooks and how to use them.

# Hooks list

**Tip:** Check the [`requirements.txt`](requirements.txt) to see the exact versions of the core dependencies being used.

## md-toc

**[Type-filters][pre-commit-type-filters]:** `file`, `markdown`, `non-executable`, `plain-text`, `text`.

It uses the [md-toc][pip-md-toc] Python package to regenerate a table of contents (TOC) for each of the selected
markdown files. It is currently hard-coded for [Gitlab-flavored markdown][gitlab-gfm]. Assuming that you set it up like

```yaml
repos:
  - repo: https://gitlab.com/psa-exe/python-pre-commit
    rev: 0.2.0
    hooks:
      - id: md-toc
        files: README.md
```

and that you have a `README.md` file like this
 
```markdown
**Table of contents:**
[](TOC)

Dummy text. It will disappear.

[](TOC)

# Section one
## Subsection of section one

# Section two
## Subsection of section two 
```

when run, this hook will modify `README.md` to

```markdown
**Table of contents:**
[](TOC)

- [Section one](#section-one)
  - [Subsection of section one](#subsection-of-section-one)
- [Section two](#section-two)
  - [Subsection of section two](#subsection-of-section-two)

[](TOC)

# Section one
## Subsection of section one

# Section two
## Subsection of section two 
``` 

**Limitations:**

* It is hard-coded for
    * writing the TOC between `[](TOC)` markers in `.md` files, and
    * parsing markdown with the `gitlab` parser,
  although [md-toc][pip-md-toc] allows these to be customized.

## poetry-export

Useful for [Poetry]-based projects that need to maintain compatibility with `requirements.txt`.
Since [version 1.0.0a4][Poetry-1.0.0a4], Poetry provides an `export` command that exports all dependencies (and, optionally, dev-dependencies as well), specified in `pyproject.toml` to `requirements.txt`.
Example configuration:

```yaml
repos:
  - repo: https://gitlab.com/psa-exe/python-pre-commit
    rev: 0.2.0
    hooks:
      - id: poetry-export
```

When [pre-commit] is run, this hook will regenerate the `requirements.txt` file.
In order to include dev-dependencies, pass `--dev` or `-d` to `args`, *i.e.:*

```yaml
repos:
  - repo: https://gitlab.com/psa-exe/python-pre-commit
    rev: 0.2.0
    hooks:
      - id: poetry-export
        args: --dev
```

**Note:** [Poetry]'s version 1.0 is in beta phase, but since the export functionality was only added in [1.0.0a4][Poetry-1.0.0a4], we are currently sticking to [1.0.0b8][Poetry-1.0.0b8].

# Future directions

* Write [py.test][python-pytest]-based tests.

---

[Poetry]: https://github.com/sdispater/poetry/
[Poetry-1.0.0a4]: https://github.com/sdispater/poetry/releases/tag/1.0.0a4
[Poetry-1.0.0b8]: https://github.com/sdispater/poetry/releases/tag/1.0.0b8
[badge-black]: https://img.shields.io/badge/code%20style-Black-black.svg
[badge-ci-security]: https://img.shields.io/badge/security-Check%20here!-yellow.svg
[badge-mit]: https://img.shields.io/badge/license-MIT-blue.svg
[badge-python]: https://img.shields.io/badge/Python-%E2%89%A53.6-blue.svg
[badge-version]: https://img.shields.io/badge/version-0.2.0%20-yellow.svg
[git-hooks]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[gitlab-gfm]: https://docs.gitlab.com/ee/user/markdown.html
[keep-a-changelog]: https://keepachangelog.com/en/1.0.0/
[license-mit]: https://opensource.org/licenses/MIT
[pip-black]: https://pypi.org/project/black/
[pip-md-toc]: https://pypi.org/project/md-toc/6.0.0
[pre-commit]: https://pre-commit.com/
[pre-commit-type-filters]: https://pre-commit.com/#filtering-files-with-types
[python-docs]: https://docs.python.org/3.6/
[python-pytest]: https://pytest.org/
[repository]: https://gitlab.com/psa-exe/python-pre-commit
[repository-latest-release]: https://gitlab.com/psa-exe/python-pre-commit/tree/0.2.0
[repository-licenses]: https://gitlab.com/psa-exe/python-pre-commit/licenses/dashboard
[repository-master]: https://gitlab.com/psa-exe/python-pre-commit/commits/master
[repository-security]: https://gitlab.com/psa-exe/python-pre-commit/security/dashboard
[semantic-versioning]: https://semver.org/spec/v2.0.0.html
[setup.py]: https://docs.python.org/3.6/distutils/setupscript.html
[virtualenv]: https://virtualenv.pypa.io/en/latest/
